import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from '../model';
import { TodoService } from './todo.service';

/**
 * Composant permettant de creer un todo.
 */
@Component({
  selector: 'app-create-todo',
  template: `
    <!-- 1- On appel la methode tryCreateTodo soit quand l'utilisateur appuie sur entrer -->
    <!-- 2 - On utilise du Two Way Data Binding pour la valeur du todo -->
    <input [(ngModel)]="todo" (keydown.enter)="tryCreateTodo()" />
    <!-- 1 - Soit au click d'un bouton -->
    <button (click)="tryCreateTodo()">CREER</button>
  `,
  styles: [`
    :host {
      display: flex;
      gap: 16px;
    }
    input {
      height: 64px;
      line-height: 64px;
      font-size: 32px;
      box-sizing: border-box;
      border-radius: 8px;
      border: solid 1px grey;
      padding: 0 16px;
    }
  `]
})
export class CreateTodoComponent {
  todo = '';

  constructor(
    private todoService: TodoService
  ) { }

  /**
   * Essaie de creer un todo, si l'input n'est pas vide.
   * @returns void
   */
  tryCreateTodo() {
    // 2 - Le Two Way Data Binding permet de recuperer la valeur ici
    // sans se preoccuper de la mettre a jour manuellement
    if (this.todo === '') return;

    this.todoService.createTodo(this.todo);

    // Et enfin, on vide l'input car le todo a bien ete cree.
    this.todo = ''
  }
}
