import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from '../model';
import { TodoService } from './todo.service';

/**
 * Composant pour afficher un todo.
 */
@Component({
  selector: 'app-todo-list-item',
  template: `
    <li *ngIf="todo !== undefined">
      <div>
        {{todo.value}}
        <button (click)="this.todoService.deleteTodo(todo.id)">SUPPRIMER</button>
      </div>
    </li>
  `,
  styles: [`
    div {
      display: flex;
      justify-content: space-between;
      align-items: center;
    }
    li {
      margin-bottom: 16px;
    }
    button {
      background: white;
      color: #6200EE;
    }
    button:hover {
      background: #6200EE11;
    }
  `]
})
export class TodoListItemComponent {
  // Le ?: ici permet de dire que le Todo peut etre `undefined`
  // Ce n'est que dans le cas ou le parent ne passerait pas de donnees dans l'input
  // Dans la pratique ca ne devrait pas arriver.
  @Input() todo?: Todo;

  constructor(
    public todoService: TodoService
  ) { }
}
