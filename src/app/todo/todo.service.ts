import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { Todo } from "../model";

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private todoSubject = new BehaviorSubject<Todo[]>([]);

  public get todos$(): Observable<Todo[]> {
    return this.todoSubject.asObservable();
  }

  /**
   * Rajoute un nouveau todo dans la liste.
   * @param value La valeur du todo a rajouter.
   */
  createTodo(value: string) {
    this.todoSubject.next([
      ...this.todoSubject.value,
      { id: Math.random(), value }
      // Equivalent a => { id: Math.random(), value: value }
    ]);

    // Meme chose avec une autre syntaxe
    // const newArray = Array.from(this.todoSubject.value);
    // newArray.push({ id: Math.random(), value });
    // this.todoSubject.next(newArray);
  }

  /**
   * Supprime un todo de la liste.
   * @param id L'id du todo a supprimer.
   */
  deleteTodo(id: number) {
    const newValue: Todo[] = this.todoSubject.value
      // On filtre les todo, pour ne garder que ceux qui ont une id differente
      .filter(todo => todo.id !== id);

    this.todoSubject.next(newValue);
  }

  /**
   * Supprime tout les todos de la liste.
   */
  deleteAllTodo() {
    this.todoSubject.next([]);
  }
}
