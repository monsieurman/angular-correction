import { Component } from '@angular/core';
import { TodoService } from './todo.service';

/**
 * Composant en charge d'afficher la liste des todo.
 */
@Component({
  selector: 'app-todo-list',
  template: `
    <ul>
      <!-- *ngFor permet de creer un nouveau composant pour chaque element du tableau -->
      <!-- On utilise | async pour s'abonner a l'observable automatiquement. -->
      <!-- Voir ci-dessous la solution sans | async -->
      <app-todo-list-item
        *ngFor="let todo of todoService.todos$ | async"
        [todo]="todo"
      >
      </app-todo-list-item>
    </ul>
  `,
  styles: [``]
})
export class TodoListComponent {
  constructor(
    public todoService: TodoService
  ) { }

  // SOLUTION SANS | async
  // // On est obliger de creer un tableau localement
  // todos: Todo[] = [];

  // constructor(
  //   public todoService: TodoService
  // ) {
  //   // Et de le changer a chaque nouvel valeur de l'observable.
  //   this.todoService.todos$
  //    .subscribe(todos => this.todos = todos);
  // }
  // Non seulement cette solution est plus longue,
  // En plus elle peut encourager la modification du tableau localement
  // Ce qui ne serait pas correct.

}
