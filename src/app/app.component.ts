import { Component } from '@angular/core';
import { Todo } from './model';
import { TodoService } from './todo/todo.service';

@Component({
  selector: 'app-root',
  template: `
    <h1>Simple Todo App</h1>
    <!-- Les composant maitrisant maintenant la donnee en passant par le service -->
    <!-- Il n'y a plus besoin de passer par des inputs et des outputs. -->
    <app-create-todo></app-create-todo>
    <app-todo-list></app-todo-list>

    <!-- Pour vider la liste on appel la methode du service. -->
    <button class="del-btn" (click)="todoService.deleteAllTodo()">VIDER</button>
  `,
  styles: [`
    :host {
      display: block;
      width: max-content;
      margin: auto;
    }
    .del-btn {
      background: #DC275E;
    }
  `]
})
export class AppComponent {
  constructor(
    public todoService: TodoService
  ) { }
}
